%% @author Aurel Paulovic <paulovic@fiit.stuba.sk> [fiit.stuba.sk/~paulovic]
%% @doc Red-Black Tree
%%
%% The module contains an example implementation of the 1st assignment for the course Distributed program systems 2014
%% 
%% TODO: missing documentation
%% TODO: actually check type specs -> currently they are not checked so maybe they are all wrong :)
%% TODO: perform some rigorous testing and eunit tests -> currently checked just using ad hoc testing methods (i.e. typed into terminal and it looked all-right :))
%% 
%% @version 0.1b
%% @copyright 2014 Aurel Paulovic

-module(tree).
-author("Aurel Paulovic").
-date({2013,02,16}).
-version("0.1b").

-export([newTree/0, toTree/1, insert/3, delete/2, size/1, min/1, max/1, get/2, toList/1, prec/2, succ/2]).


-record(node, {
	  key :: any(),
	  val :: any(),
	  left = 'nil' :: treeNode() | 'nil',
	  right = 'nil' :: treeNode() | 'nil',
	  colour = 'red' :: 'red' | 'black' | 'double_black' | 'double_black_nil'
}).

-record(tree, {
	  root = 'nil' :: treeNode() | 'nil',
	  size = 0 :: non_neg_integer()
}).

-type treeNode() :: #node{}.
-opaque tree() :: #tree{}.

-type direction() :: 'left' | 'right'.
-type insertAction() :: 'added' | 'updated'.
-type traceElement() :: {direction(), treeNode()}.
-type trace() :: list(traceElement()).
-type balanceResult() :: 'balanced' | 'mayNeedRebalance'.
-type deleteAction() :: 'notFound' | 'deleted'.
-type inorderDirection() :: 'down' | 'upFromLeft' | 'upFromRight'.
-type returnOption() :: {'ok', any()} | 'nil'.

-export_type([tree/0]).

-define(IS_BLACK(X), X#node.colour == 'black').
-define(IS_RED(X), X#node.colour == 'red').
-define(IS_BLACK(X,Y), (X#node.colour == 'black' andalso  Y#node.colour == 'black')).
-define(IS_BLACK(X,Y,Z), (X#node.colour == 'black' andalso  Y#node.colour == 'black' andalso Z#node.colour == 'black')).
-define(IS_DOUBLE_BLACK(X), (X#node.colour == 'double_black' orelse X#node.colour == 'double_black_nil')).

-spec newTree() -> tree().
newTree() ->
    #tree{}.

-spec toTree(dict()) -> tree().
toTree(Dict) ->
    dict:fold(fun(Key, Val, Acc) ->
		 insert(Acc, Key, Val)
	      end,
	      #tree{},
	      Dict).

-spec insert(tree(), Key::any(), Val::any()) -> tree().
insert(#tree{root = Node, size = Size}, Key, Val) ->
    case insertNode(Node, Key, Val, []) of
	{added, NewRoot} ->
	    #tree{root = NewRoot, size = Size + 1};
	{updated, NewRoot} ->
	    #tree{root = NewRoot, size = Size}
    end.


-spec insertNode(CurNode::treeNode(), Key::any(), Val::any(), Trace::trace()) -> {insertAction(), treeNode()}.
insertNode(CurNode = #node{key = CurKey, left = Left}, Key, Val, Trace) when Key < CurKey -> % go to left child
    insertNode(Left, Key, Val, [{left, CurNode} | Trace]);
insertNode(CurNode = #node{key = CurKey, right = Right}, Key, Val, Trace) when CurKey < Key -> % go to right child
    insertNode(Right, Key, Val, [{right, CurNode} | Trace]);
insertNode(CurNode = #node{key = Key}, Key, Val, Trace) -> % update current node
    {updated, rebuildTrace([CurNode#node{val = Val} | Trace], nil)};
insertNode(nil, Key, Val, Trace) -> % create new leaf
    {added, rebuildAndBalanceTrace([{left, #node{key = Key, val = Val}} | Trace], nil)}.

-spec rebuildTrace(trace(), Acc::treeNode()) -> treeNode().
rebuildTrace([], Acc) ->
    Acc#node{colour = 'black'};
rebuildTrace([{left, Node} | Trace], Acc) ->
    rebuildTrace(Trace, Node#node{left = Acc});
rebuildTrace([{right, Node} | Trace], Acc) ->
    rebuildTrace(Trace, Node#node{right = Acc});
rebuildTrace([Node = #node{} | Trace], _Acc) ->
    rebuildTrace(Trace, Node).


-spec rebuildAndBalanceTrace(trace(), Acc::treeNode()) -> treeNode().
rebuildAndBalanceTrace([], Acc) ->
    Acc#node{colour = 'black'};
rebuildAndBalanceTrace([{_Dir, N} | []], Acc) -> % current node is root
    rebuildAndBalanceTrace([], N#node{left = Acc});
rebuildAndBalanceTrace([{NDir, N} | [{PDir, P} | []]], Acc) -> 
    NewN = mergeWithChild(NDir, N, Acc),
    NewP = mergeWithChild(PDir, P, NewN),
    {balanced, BalancedNewP} = balance(NewP),
    rebuildAndBalanceTrace([], BalancedNewP);
rebuildAndBalanceTrace([{NDir, N} | [{PDir, P} | [{GPDir, GP} | Trace]]], Acc) -> 
    NewN = mergeWithChild(NDir, N, Acc),
    NewP = mergeWithChild(PDir, P, NewN),
    NewGP = mergeWithChild(GPDir, GP, NewP),
    case balance(NewGP) of
	{mayNeedRebalance, BalancedNewGP} -> 
	    rebuildAndBalanceTrace([{left, BalancedNewGP} | Trace], BalancedNewGP#node.left);
	{balanced, BalancedNewGP} ->
	    rebuildTrace(Trace, BalancedNewGP)
    end.

-spec mergeWithChild(direction(), Parent::treeNode(), Child::treeNode()) -> treeNode().
mergeWithChild(left, Parent, Child) ->
    Parent#node{left = Child};
mergeWithChild(right, Parent, Child) ->
    Parent#node{right = Child}.
		    

-spec balance(treeNode()) -> {balanceResult(), treeNode()}.
% Parent is red, our node is red (GrandParent must be black) and Uncle is red -> recolour GrandParent, Parent and Uncle
% !! This can push the color disruption to the top of the subtree -> we will need to recursively ballance the tree
balance(GP = #node{left = P = #node{colour = 'red', left = #node{colour = 'red'}}, right = U = #node{colour = 'red'}}) -> % left parent, left child
    {mayNeedRebalance, GP#node{colour = 'red', left = P#node{colour = 'black'}, right = U#node{colour = 'black'}}};
balance(GP = #node{left = P = #node{colour = 'red', right = #node{colour = 'red'}}, right = U = #node{colour = 'red'}}) -> % left parent, right child
    {mayNeedRebalance, GP#node{colour = 'red', left = P#node{colour = 'black'}, right = U#node{colour = 'black'}}};
balance(GP = #node{left = U = #node{colour = 'red'}, right = P = #node{colour = 'red', left = #node{colour = 'red'}}}) -> % right parent, left child
    {mayNeedRebalance, GP#node{colour = 'red', left = U#node{colour = 'black'}, right = P#node{colour = 'black'}}};
balance(GP = #node{left = U = #node{colour = 'red'}, right = P = #node{colour = 'red', right = #node{colour = 'red'}}}) -> % right parent, right child
    {mayNeedRebalance, GP#node{colour = 'red', left = U#node{colour = 'black'}, right = P#node{colour = 'black'}}};

% Parent is red, our node is red (GrandParent must be black) and Uncle is black (or nil), Parent is on the same side as node -> rotate and recolour Parent and GrandParent
balance(GP = #node{left = P = #node{colour = 'red', left = #node{colour = 'red'}}, right = _U}) -> % left parent, left child
    {balanced, P#node{colour = 'black', right = GP#node{colour = 'red', left = P#node.right}}};
balance(GP = #node{left = _U, right = P = #node{colour = 'red', right = #node{colour = 'red'}}}) -> % right parent, right child
    {balanced, P#node{colour = 'black', left = GP#node{colour = 'red', right = P#node.left}}};

% Parent is red, our node is red (GrandParent must be black) and Uncle is black (or nil), Parent is on the opposite side as node -> rotate and recolour Node and GrandParent
balance(GP = #node{left = P = #node{colour = 'red', right = N = #node{colour = 'red'}}, right = _U}) -> % left parent, right child
    {balanced, N#node{colour = 'black', left = P#node{right = N#node.left}, right = GP#node{colour = 'red', left = N#node.right}}};
balance(GP = #node{left = _U, right = P = #node{colour = 'red', left = N = #node{colour = 'red'}}}) -> % right parent, left child
    {balanced, N#node{colour = 'black', left = GP#node{colour = 'red', right = N#node.left}, right = P#node{left = N#node.right}}}; 

% if the Parent has no GrandParent (i.e. is root) -> recolour the root to black
balance(P = #node{colour = 'red', left = #node{colour = 'red'}}) -> 
    {balanced, P#node{colour = 'black'}};
balance(P = #node{colour = 'red', right = #node{colour = 'red'}}) ->
    {balanced, P#node{colour = 'black'}};   

% everything else preserves the color properties of RB-tree
balance(N) ->
    {balanced, N}.


-spec delete(tree(), Key::any()) -> tree().
delete(Tree = #tree{root = Node, size = Size}, Key) -> 
    case deleteNode(Node, Key, []) of
	{deleted, NewRoot} ->
	    #tree{root = NewRoot, size = Size - 1};
	{notFound, _} ->
	    Tree
    end.

-spec deleteNode(treeNode(), any(), trace()) -> {deleteAction(), treeNode()}. 
deleteNode(nil, _Key, _Trace) ->
    {notFound, nil};
deleteNode(Node = #node{key = Key, colour = 'red'}, Key, Trace) -> % deleting red node is easy
    {deleted, deleteRedNode(Node, Trace)};
deleteNode(Node = #node{key = Key, colour = 'black'}, Key, Trace) -> % deleting black node is hard 
    {deleted, deleteBlackNode(Node, Trace)};
deleteNode(CurNode = #node{key = CurKey, left = Left}, Key, Trace) when Key < CurKey ->
    deleteNode(Left, Key, [{left, CurNode} | Trace]);
deleteNode(CurNode = #node{key = CurKey, right = Right}, Key, Trace) when CurKey < Key ->
    deleteNode(Right, Key, [{right, CurNode} | Trace]).

-spec deleteRedNode(treeNode(), trace()) -> treeNode().
deleteRedNode(#node{left = nil, right = nil}, Trace) -> % red leaf node - just delete it
    case Trace of
	[] -> % the deleted node was the only node - root
	    nil;
	[{left, Node} | Rest] ->
	    rebuildTrace([{left, Node#node{left = nil}} | Rest], nil);
	[{right, Node} | Rest] ->
	    rebuildTrace([{right, Node#node{right = nil}} | Rest], nil)
    end;
deleteRedNode(#node{left = nil, right = RChild}, Trace) -> % red node with one child -> delete it and put the child in its place instead (color will be ok)
    case Trace of
	[] ->
	    RChild;
	Trace ->
	    rebuildTrace(Trace, RChild)
    end;
deleteRedNode(#node{left = LChild, right = nil}, Trace) -> % red node with one child -> delete it and put the child in its place instead (colors will be ok)
    case Trace of
	[] ->
	    LChild;
	Trace ->
	    rebuildTrace(Trace, LChild)
    end;
deleteRedNode(Node = #node{}, Trace) -> % red node with both children -> find successor, relabel and delete successor
    {Succ, TraceToSucc} = getSuccessorAndTrace(Node#node.right, []),
    RelabeledNode = Node#node{key = Succ#node.key, val = Succ#node.val}, % colour is red
    NewTrace = TraceToSucc ++ [{right, RelabeledNode} | Trace],
    case Succ#node.colour of
	black ->
	    deleteBlackNode(Succ, NewTrace);
	red ->
	    deleteRedNode(Succ, NewTrace)
    end.
    
-spec getSuccessorAndTrace(treeNode(), trace()) -> {treeNode(), trace()}.
getSuccessorAndTrace(Node = #node{left = nil}, Trace) ->
    {Node, Trace};
getSuccessorAndTrace(Node = #node{left = LChild}, Trace) ->
    getSuccessorAndTrace(LChild, [{left, Node} | Trace]).

-spec deleteBlackNode(treeNode(), trace()) -> treeNode().
% node is black and has no children -> remove it but introduce double_black nil node
deleteBlackNode(#node{left = nil, right = nil}, Trace) ->
    NewNilNode =  #node{left = nil, right = nil, key = nil, val = nil, colour = 'double_black_nil'},
    if Trace == [] -> nil;
       true ->
	    fixDoubleBlack(fixTrace(Trace, NewNilNode))
    end;

% node has only one child, which is red -> delete the node, move the child in its place and recolour it to black, to fix preserve black height
deleteBlackNode(#node{left = nil, right = RChild = #node{colour = 'red'}}, Trace) ->
    rebuildTrace(Trace, RChild#node{colour = 'black'});
deleteBlackNode(#node{left = LChild = #node{colour = 'red'}, right = nil}, Trace) -> 
    rebuildTrace(Trace, LChild#node{colour = 'black'});

% node has only one child, but it is black -> delete the node, move the child in its place, and add double black color, which we will fix later
deleteBlackNode(#node{left = nil, right = RChild}, Trace) -> 
    NewChild = RChild#node{colour = 'double_black'},
    fixDoubleBlack(fixTrace(Trace, NewChild));

deleteBlackNode(#node{left = LChild, right = nil}, Trace) ->
    NewChild = LChild#node{colour = 'double_black'},
    fixDoubleBlack(fixTrace(Trace, NewChild));

% node has 2 children -> find successor, relabel current node (with sucessor values) and colour it black to preserve black height, then delete the sucessor
deleteBlackNode(Node = #node{}, Trace) ->
    {Succ, TraceToSucc} = getSuccessorAndTrace(Node#node.right, []),
    RelabeledNode = Node#node{key = Succ#node.key, val = Succ#node.val}, % code is black
    NewTrace = TraceToSucc ++ [{right, RelabeledNode} | Trace],
    case Succ#node.colour of
	black ->
	    deleteBlackNode(Succ, NewTrace);
	red ->
	    deleteRedNode(Succ, NewTrace)
    end.

-spec fixTrace(trace(), treeNode()) -> trace().
fixTrace([], Node) ->
    [Node | []];
fixTrace([{left, Parent} | Rest], Node) ->
    [{left, Parent#node{left = Node}} | Rest];
fixTrace([{right, Parent} | Rest], Node) ->
    [{right, Parent#node{right = Node}} | Rest].

-spec fixDoubleBlack(trace()) -> treeNode().
% Node is the root
fixDoubleBlack([Node | []]) ->
    if Node#node.colour == 'double_black_nil' -> % nil node
	    nil;
       true -> % black, red, double_black 
	    Node#node{colour = 'black'}
    end;

% double-black Node has red parent and black brother with no children -> colour the parent black and the brother red
fixDoubleBlack([{left, Parent = #node{left = Node, right = Sibling = #node{left = nil, right = nil}}} | Trace]) when ?IS_RED(Parent), ?IS_DOUBLE_BLACK(Node) ->
    NewSibling = Sibling#node{colour = 'red'},
    NewParent = Parent#node{colour = 'black', left = removeDoubleBlack(Node), right = NewSibling},
    rebuildTrace(Trace, NewParent);
fixDoubleBlack([{right, Parent = #node{right = Node, left = Sibling = #node{left = nil, right = nil}}} | Trace]) when ?IS_RED(Parent), ?IS_DOUBLE_BLACK(Node) ->
    NewSibling = Sibling#node{colour = 'red'},
    NewParent = Parent#node{colour = 'black', left = NewSibling, right = removeDoubleBlack(Node)},
    rebuildTrace(Trace, NewParent);

% Double-black node has red parent and black nephews -> recolour parent to black and recolour sibling to red (Node is guaranteed to have at least one child)
fixDoubleBlack([{left, Parent = #node{colour = 'red', left = Node, right = Sibling = #node{right = RNephew, left = LNephew}}} | Trace]) when ?IS_BLACK(RNephew, LNephew), ?IS_DOUBLE_BLACK(Node) ->
    NewSibling = Sibling#node{colour = 'red'}, % Sibling was black
    NewParent = Parent#node{colour = 'black', left = removeDoubleBlack(Node), right = NewSibling},
    rebuildTrace(Trace, NewParent);
fixDoubleBlack([{right, Parent = #node{colour = 'red', right = Node, left = Sibling = #node{right = RNephew, left = LNephew}}} | Trace]) when ?IS_BLACK(RNephew, LNephew), ?IS_DOUBLE_BLACK(Node) ->
    NewSibling = Sibling#node{colour = 'red'},
    NewParent = Parent#node{colour = 'black', left = NewSibling, right = removeDoubleBlack(Node)},
    rebuildTrace(Trace, NewParent);

% Double-black Node has black parent, black nephews and black sibling -> move double black to parent and change sibling to red
fixDoubleBlack([{left, Parent = #node{colour = 'black', left = Node, right = Sibling = #node{right = RNephew, left = LNephew}}} | Trace]) when ?IS_BLACK(RNephew, LNephew, Sibling), ?IS_DOUBLE_BLACK(Node) ->
    NewNode = Node#node{colour = 'black'},
    NewSibling = Sibling#node{colour = 'red'},
    NewParent = Parent#node{colour = 'double_black', left = NewNode, right = NewSibling},
    fixDoubleBlack(fixTrace(Trace, NewParent));
fixDoubleBlack([{right, Parent = #node{colour = 'black', right = Node, left = Sibling = #node{right = RNephew, left = LNephew}}} | Trace]) when ?IS_BLACK(RNephew, LNephew, Sibling), ?IS_DOUBLE_BLACK(Node) ->
    NewNode = Node#node{colour = 'black'},
    NewSibling = Sibling#node{colour = 'red'},
    NewParent = Parent#node{colour = 'double_black', left = NewSibling, right = NewNode},
    fixDoubleBlack(fixTrace(Trace, NewParent));

% double-black Node is a left child of red parent, but right nephew is red (maybe left too but we dont care) -> rotate, recolour sibling to red, parent to black and the right red newphew black
fixDoubleBlack([{left, Parent = #node{left = Node, right = Sibling = #node{right = RNephew}}} | Trace]) when ?IS_RED(Parent), ?IS_RED(RNephew), ?IS_DOUBLE_BLACK(Node) ->
    NewRNephew = RNephew#node{colour = 'black'},
    NewParent = Parent#node{colour = 'black', left = removeDoubleBlack(Node), right = Sibling#node.left},
    NewSibling = Sibling#node{colour = 'red', left = NewParent, right = NewRNephew},
    rebuildTrace(Trace, NewSibling);
fixDoubleBlack([{right, Parent = #node{right = Node, left = Sibling = #node{left = LNephew}}} | Trace]) when ?IS_RED(Parent), ?IS_RED(LNephew), ?IS_DOUBLE_BLACK(Node) ->
    NewLNephew = LNephew#node{colour = 'black'},
    NewParent = Parent#node{colour = 'black', left = Sibling#node.right, right = removeDoubleBlack(Node)},
    NewSibling = Sibling#node{colour = 'red', left = NewLNephew, right = NewParent},
    rebuildTrace(Trace, NewSibling);

% double-black Node has red Sibling -> recolour Sibling black, Parent to red, rotate -> fix DB again
fixDoubleBlack([{left, Parent = #node{left = Node, right = Sibling}} | Trace]) when ?IS_RED(Sibling), ?IS_DOUBLE_BLACK(Node) ->
    NewParent = Parent#node{colour = 'red', right = Sibling#node.left},
    NewSibling = Sibling#node{colour = 'black', left = NewParent},
    NewTrace = [{left, NewParent} | [{left, NewSibling} | Trace]],
    fixDoubleBlack(NewTrace);
fixDoubleBlack([{right, Parent = #node{right = Node, left = Sibling}} | Trace]) when ?IS_RED(Sibling), ?IS_DOUBLE_BLACK(Node) ->
    NewParent = Parent#node{colour = 'red', left = Sibling#node.right},
    NewSibling = Sibling#node{colour = 'black', right = NewParent},
    NewTrace = [{right, NewParent} | [{right, NewSibling} | Trace]],
    fixDoubleBlack(NewTrace);

% double-black Node is left child of black Parent, has black Sibling and red right Nephew -> colour the Newphew to black, rotate
fixDoubleBlack([{left, Parent = #node{left = Node, right = Sibling = #node{right = RNephew}}} | Trace]) when ?IS_BLACK(Parent, Sibling), ?IS_DOUBLE_BLACK(Node), ?IS_RED(RNephew) ->
    NewRNephew = RNephew#node{colour = 'black'},
    NewParent = Parent#node{left = removeDoubleBlack(Node), right = Sibling#node.left},
    NewSibling = Sibling#node{left = NewParent, right = NewRNephew},
    rebuildTrace(Trace, NewSibling);
fixDoubleBlack([{right, Parent = #node{right = Node, left = Sibling = #node{left = LNephew}}} | Trace]) when ?IS_BLACK(Parent, Sibling), ?IS_DOUBLE_BLACK(Node), ?IS_RED(LNephew) ->
    NewLNephew = LNephew#node{colour = 'black'},
    NewParent = Parent#node{left = Sibling#node.right, right = removeDoubleBlack(Node)},
    NewSibling = Sibling#node{left = NewLNephew, right = NewParent},
    rebuildTrace(Trace, NewSibling).

-spec removeDoubleBlack(treeNode()) -> treeNode().
removeDoubleBlack(#node{colour = 'double_black_nil'}) ->
    nil;
removeDoubleBlack(Node = #node{colour = 'double_black'}) ->
    Node#node{colour = 'black'}.

-spec size(tree()) -> non_neg_integer().
size(#tree{size = Size}) ->
    Size.

-spec min(tree()) -> returnOption().
min(#tree{size = 0}) ->
    nil;
min(#tree{root = Node}) ->
    {ok, p_min(Node)}.

-spec p_min(treeNode()) -> any().
p_min(#node{left = nil, key = Key}) ->
    Key;
p_min(#node{left = Node}) ->
    p_min(Node).

-spec max(tree()) -> returnOption().
max(#tree{size = 0}) ->
    nil;
max(#tree{root = Node}) ->
    {ok, p_max(Node)}.

-spec p_max(treeNode()) -> any().
p_max(#node{right = nil, key = Key}) ->
    Key;
p_max(#node{right = Node}) ->
    p_max(Node).

-spec get(tree(), any()) -> returnOption().
get(#tree{root = Node}, Key) ->
   case p_get(Node, Key) of
       nil ->
	   nil;
       Val ->
	   {ok, Val}
    end.

-spec p_get(treeNode(), any()) -> 'nil' | any().
p_get(#node{key = Key, val = Val}, Key) ->
    Val;
p_get(#node{key = CurKey, left = Left}, Key) when Key < CurKey ->
    p_get(Left, Key);
p_get(#node{key = CurKey, right = Right}, Key) when CurKey < Key ->
    p_get(Right, Key);
p_get(nil, _Key) ->
    nil.

-spec toList(tree()) -> list({any(), any()}).
toList(#tree{size = 0}) ->
    [];
toList(#tree{root = Node}) ->
    reverseInorder(Node, down, [], []).

-spec reverseInorder('nil' | treeNode(), inorderDirection(), trace(), list({any(), any()})) -> list({any(), any()}).
% we arrived in empty right child -> go back to our parent (we are right child)
reverseInorder(nil, down, [{right, Node} | Trace], Acc) ->
    reverseInorder(Node, upFromRight, Trace, Acc);
% we came from right subtree -> put current node in Acc and drill into left subtree
reverseInorder(Node, upFromRight, Trace, Acc) ->
    reverseInorder(Node#node.left, down, [{left, Node} | Trace], [{Node#node.key, Node#node.val} | Acc]);
% we arrived in empty left child -> go back to our parent (we are left child)
reverseInorder(nil, down, [{left, Node} | Trace], Acc) ->
    reverseInorder(Node, upFromLeft, Trace, Acc);
% we came from left subtree -> we processed current node and both subtrees -> go back up to our parent (we are left child)
reverseInorder(_Node, upFromLeft, [{left, Parent} | Trace], Acc) -> 
    reverseInorder(Parent, upFromLeft, Trace, Acc);
% we came from left subtree -> we processed current node and both subtrees -> go back up to our parent (we are right child)
reverseInorder(_Node, upFromLeft, [{right, Parent} | Trace], Acc) ->
    reverseInorder(Parent, upFromRight, Trace, Acc);
% we came from left subtree but have no parent in trace -> we must be root so this is the end
reverseInorder(_Node, upFromLeft, [], Acc) -> 
    Acc;
% by default, drill down into right subtree
reverseInorder(Node, down, Trace, Acc) -> 
    reverseInorder(Node#node.right, down, [{right, Node} | Trace], Acc).


-spec prec(tree(), any()) -> returnOption().
prec(#tree{size = 0}, _Key) ->
    nil;
prec(#tree{root = Node}, Key) ->
    p_prec(Node, Key, nil).

-spec p_prec(treeNode(), any(), treeNode()) -> 'nil' | returnOption().
p_prec(#node{key = CurKey, left = Left}, Key, CandidateParent) when Key < CurKey ->
    p_prec(Left, Key, CandidateParent);
p_prec(CurNode = #node{key = CurKey, right = Right}, Key, _CandidateParent) when CurKey < Key ->
    p_prec(Right, Key, CurNode);
p_prec(#node{key = Key, left = nil}, Key, nil) ->
    nil;
p_prec(#node{key = Key, left = nil}, Key, CandidateParent) ->
    {ok, CandidateParent#node.key};
p_prec(#node{key = Key, left = Left}, Key, _CandidateParent) ->
    p_prec(Left);
p_prec(nil, _Key, _CandidateParent) ->
    nil.

-spec p_prec(treeNode()) -> returnOption().
p_prec(#node{right = nil, key = CurKey}) ->
    {ok, CurKey};
p_prec(#node{right = Right}) ->
    p_prec(Right).


-spec succ(tree(), any()) -> returnOption().
succ(#tree{size = 0}, _Key) ->
    nil;
succ(#tree{root = Node}, Key) ->
    p_succ(Node, Key, nil).

-spec p_succ(treeNode(), any(), treeNode()) -> 'nil' | returnOption().
p_succ(CurNode = #node{key = CurKey, left = Left}, Key, _CandidateParent) when Key < CurKey ->
    p_succ(Left, Key, CurNode);
p_succ(#node{key = CurKey, right = Right}, Key, CandidateParent) when CurKey < Key ->
    p_succ(Right, Key, CandidateParent);
p_succ(#node{key = Key, right = nil}, Key, nil) ->
    nil;
p_succ(#node{key = Key, right = nil}, Key, CandidateParent) ->
    {ok, CandidateParent#node.key};
p_succ(#node{key = Key, right = Right}, Key, _CandidateParent) ->
    p_succ(Right);
p_succ(nil, _Key, _CandidateParent) ->
    nil.

-spec p_succ(treeNode()) -> returnOption().
p_succ(#node{left = nil, key = CurKey}) ->
    {ok, CurKey};
p_succ(#node{left = Left}) ->
    p_succ(Left).



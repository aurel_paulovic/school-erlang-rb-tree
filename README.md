# RB-Tree #
This is an implementation of a Red-Black Tree in Erlang. The implementation was developed as an example implementation of an assignment for the course Distributed Program Systems 2014 at [FIIT STU](http://www.fiit.stuba.sk/).

The implementation is tail-recursive and features a working implementation of delete (something that Mr. Okasaki did not do :D so you cant really find it on the web).

There is no good documentation and the tests are missing, so use at your own risk. Also, a real-world implementation would not need to use tail-recursion since the tree tends to be shallow, but for the sake of the example I did it this way.

## The assignment ##
Implement a Red-Black Tree in Erlang. The tree must be represented using tuples and you can not use existing functions in Erlang's modules with the exception of guard expressions. In the implementation of the function toTree/1 you are allowed to use functions from the module Dict. (i.e. you can use e.f. is_number/1 (guard), but you can not use lists:delete/2 or length/1 (in other place as a guard)) 

The tree is defined as a balanced RB-Tree of key-value pairs ordered by keys in ascending order. The keys must be unique (uniqueness is defined by =:=). The tree is indexed by keys.

Following is the list of required functions operating on the tree:

* `newTree/0 -> tree()` - creates new empty RB-Tree
* `insert(tree(), key(), value()) -> tree()` - insert key-value pair into tree, update the value if the key is already present in the tree, return the new tree
* `delete(tree(), key()) -> tree() - delete key (if present) and its corresponding value from the tree, return the new tree
* `prec(tree(), key()) -> {ok, key()} | nil` - return the predecessor of a key in the tree or nil, if there is no predecessor or the key is not present in the tree
* `succ(tree(), key()) -> {ok, key()} | nil` - return the successor of a key in the tree or nil, if there is no successor or the key is not present in the tree
* `toList(tree()) -> list({key(), val()})` - return an ordered list of key-value pairs that are present in the tree
* `max(tree()) -> {ok, key()} | nil` - return the biggest key (the last according to the ordering) in the tree, or nil if the tree is empty
* `min(tree()) -> {ok, key()} | nil` - return the smallest key (the first according to the ordering) in the tree, or nil if the tree is empty
* `get(tree(), key()) -> {ok, val()} | nil` - return the value associated with the key, or nil if the key is not present in the tree


The predecessor of a key A is the largest key B that is smaller than key A.
The successor of a key A is the smallest key B that is larger than key A.